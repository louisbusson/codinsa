taille = int(input())
longueur = int(input())

matrice = []

for i in range(taille):
   matrice.append(input().split(" "))

x, y = list(map(int, input().split(" ")))
ax, cx = list(map(int, input().split(" ")))
ay, cy = list(map(int, input().split(" ")))

path = []

for i in range(longueur-1):
   x = (ax * x + cx) % taille
   y = (ay * y + cy) % taille
   if matrice[x][y] not in path : path.append(matrice[x][y])

for i in path:
   print(i)