N, M = list(map(int, input().split(" ")))
matrice = []
for i in range(N):
   matrice.append([int(i) for i in input()])

def move(current, check, arrivee):
   path = []
   if(check == arrivee):
      return True
   elif matrice[check[0]][check[1]] != matrice[arrivee[0]][arrivee[1]]:
      return False
   elif check[0]+1 != current[0] and check[0]+1 in range(N) and move(check, (check[0]+1, check[1]), arrivee):
      return True
   elif check[0]-1 != current[0] and check[0]-1 in range(N) and move(check, (check[0]-1, check[1]), arrivee):
      return True
   elif check[1]+1 != current[1] and check[1]+1 in range(M) and move(check, (check[0], check[1]+1), arrivee):
      return True
   elif check[1]-1 != current[1] and check[1]-1 in range(M) and move(check, (check[0], check[1]-1), arrivee):
      return True
   return False

nb_question = int(input())

for i in range(nb_question):
   x1, y1, x2, y2 = list(map(int, input().split(" ")))
   if move((N+1, M+1), (x1, y1), (x2, y2)):
      print("OUI")
   else:
      print("NON")

