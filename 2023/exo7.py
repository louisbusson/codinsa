entry_list = [int(i) for i in input()]
number_list =  list(set(entry_list))
number_list.sort()
occurence = []
for i in number_list:
   occurence.append(entry_list.count(i))

i=1
found=True
while found:
   digit = [int(j) for j in str(i)]
   for j in digit:
      if j not in number_list or digit.count(j) > occurence[number_list.index(j)]:
         found=False
   i+= 1

print(i-1)