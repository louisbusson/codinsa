
data = input().split(" ")
pile = []
i = 0
count = 0
mid = False

while i < len(data)-1:
   if(data[i] == "Bienvenue"):
      if(data[i+1] == "a"):
         if(data[i+2] == "Rouen"):
            i+=3
            count += 1
            mid = True
         else:
            if mid: break
            pile.append(data[i]+" "+data[i+1])
            i+=2
      else:
         if mid: break
         pile.append(data[i])
         i+=1

if mid:
   while i < len(data):
      current = pile.pop()
      if i<len(data)-1 and current+" "+data[i] == "Bienvenue a Rouen":
         i += 1
         count+= 1
      elif i<len(data)-2 and current+" "+data[i]+" "+data[i+1] == "Bienvenue a Rouen":
         i += 2
         count += 1
      else:
         count=0
         break

   print(count)
else:
   print(0)