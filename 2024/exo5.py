N = int(input())
motif_len = 0
grid = []

for i in range(5):
   line = input()
   grid.append(line)
   current_motif = 0
   for i in range(0, N):
      if (line.count(line[:i])*i) == N:
         current_motif = i
         break

   if current_motif > motif_len:
      motif_len = current_motif

print(motif_len)

for i in range(5):
   print(grid[i][:motif_len])