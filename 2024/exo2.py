N = int(input())//2

somme_gauche = 0
somme_droite = 0

for i in range(N):
   data = int(input())
   somme_gauche += data*(N-i)

for i in range(N):
   data = int(input())
   somme_droite += data*(i+1)

if somme_gauche > somme_droite:
   print("Gauche")
else:
   print("Droite")
