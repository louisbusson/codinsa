nb_act = int(input())
all_act = input()
act=[int(x) for x in all_act.split(' ')]
nb_rep = int(input())
all_rep = input()
rep=[int(x) for x in all_rep.split(' ')]
rep.append(1440)

for i in range(nb_rep+1):
   duration = 0
   if(i==0):
      duration = rep[i]
   else:
      duration = rep[i]-rep[i-1]

   total_act_duration = 0
   while 1:
      if len(act) != 0 and (act[0]+total_act_duration) <= duration:
         total_act_duration += act[0]
         act.pop(0)
      else:
         break

if len(act) == 0:
   print("Tout est faisable !")
else:
   print("On va devoir abandonner {nb} activite(s) ...".format(nb=len(act)))