Ce repo contient mes solutions aux exercices de qualification de Cod'INSA pour les éditions 2023 et 2024
Cod'INSA est un concours de programmation inter-INSA qui se déroule chaque année.
La phase de qualification dure 3h durant lesquelles les participants doivent résoudre le plus d'exercices.

Plus d'informations sur [le site de Cod'INSA](https://codinsa.org)